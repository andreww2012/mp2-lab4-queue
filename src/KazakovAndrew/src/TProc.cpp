#ifndef __TPROC_CPP__
#define __TPROC_CPP__

#include <cstdlib>
#include "TProc.h"

TProc::TProc(int q2) : q2(q2), jobDuration(JOB_DURATION_DISABLED), isWorkingStatus(false) {

	if (q2 < 0 || q2 > MAX_CPU_PERFORMANCE) {
		throw 1;
	}

}

void TProc::cycle() {

	// ��������� �� ������� ������
	if (jobDuration == JOB_DURATION_DISABLED && (rand() % MAX_CPU_PERFORMANCE < q2)
		|| !--jobDuration) {
		isWorkingStatus = false;
	}

}

void TProc::startNewJob(int duration) {

	if (duration < 0 && duration != JOB_DURATION_DISABLED) {
		throw 2;
	}

	isWorkingStatus = true;

}

bool TProc::isWorking() {

	return isWorkingStatus;

}

#endif
#include "gtest/gtest.h"
#include "TQueue.h"

//
// Creation
//

TEST(Queue, can_create_queue_of_elements_of_default_type) {
  ASSERT_NO_THROW(TQueue<> queue);
}

TEST(Queue, can_create_queue_of_ints) {
  ASSERT_NO_THROW(TQueue<int> queue);
}

TEST(Queue, can_create_queue_of_bools) {
  ASSERT_NO_THROW(TQueue<bool> queue);
}

TEST(Queue, can_create_queue_with_specified_length) {
  ASSERT_NO_THROW(TQueue<int> queue(5));
}

TEST(Queue, cant_create_queue_with_negative_size) {
  ASSERT_ANY_THROW(TQueue<int> queue(-5));
}

//
// Put/get
//

TEST(Queue, can_put_elem_in_queue_without_specifying_its_size) {
  TQueue<int> queue;

  ASSERT_NO_THROW(queue.put(10));
}

TEST(Queue, can_put_elem_in_queue_with_specified_size) {
  TQueue<int> queue(2);

  ASSERT_NO_THROW(queue.put(10));
}

TEST(Queue, can_get_elem) {
  TQueue<int> queue(2);
  queue.put(7);

  ASSERT_NO_THROW(queue.get());
}

TEST(Queue, put_element_is_correct) {
  TQueue<int> queue(2);
  queue.put(6);

  EXPECT_EQ(queue.get(), 6);
}

//
// Empty/full
//

TEST(Queue, empty_queue_is_empty) {
  TQueue<int> queue(2);

  EXPECT_EQ(queue.isEmpty(), true);
}

TEST(Queue, not_full_queue_is_not_full) {
  TQueue<int> queue(2);
  queue.put(3);

  EXPECT_EQ(queue.isFull(), false);
}

TEST(Queue, not_empty_queue_is_not_empty) {
  TQueue<int> queue(2);
  queue.put(3);

  EXPECT_EQ(queue.isEmpty(), false);
}

TEST(Queue, full_queue_is_full) {
  TQueue<int> queue(2);
  queue.put(3);
  queue.put(4);

  EXPECT_EQ(queue.isFull(), true);
}

// Other
TEST(Queue, cant_put_elem_when_queue_is_full) {
  TQueue<int> queue(2);
  queue.put(3);
  queue.put(4);

  ASSERT_ANY_THROW(queue.put(5));
}

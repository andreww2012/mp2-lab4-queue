#ifndef __TPROC_H__
#define __TPROC_H__

// Просто флаг, обозначающий, что длительность такта для текущего задания
// будет определяться рандомно (исходя из q2). Не должно быть равно нулю
#define JOB_DURATION_DISABLED -1
#define MAX_CPU_PERFORMANCE 100

class TProc {
public:
  explicit TProc(int q2);
  void cycle();
  void startNewJob(int duration = JOB_DURATION_DISABLED);
  bool isWorking();

protected:
  bool isWorkingStatus;
  // "Производительность" процессора, принадлежит [0; MAX_CPU_PERFORMANCE]
  int q2;
  int jobDuration; // длительность текущего задани¤ в тактах
};

#endif

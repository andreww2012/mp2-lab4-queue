#ifndef __TJOBSTREAM_H__
#define __TJOBSTREAM_H__

#include "TJob.h"
#include "TQueue.h"
#include "TProc.h"

#define MAX_FLOW_INTENSITY 100
#define MAX_JOB_DURATION 100000
#define DEFAULT_CPU_CYCLES 1000000

class TJobStream {
public:
  TJobStream(int q1, int q2, int q3, int queueLen);
  void startJobStream(int cycles = DEFAULT_CPU_CYCLES);
  void showStats();

private:
  TProc CPU;
  TQueue<TJob> jobsQueue;
  int q1; // »нтенсивность потока заданий, [0; MAX_FLOW_INTENSITY]
  int q3; // ћаксимальна¤ длительность задани¤, [0; MAX_JOB_DURATION]

  int currID; // пор¤дковый номер задани¤
  int stats_all_cycles, stats_useful_cycles, stats_refuses; // статистическа¤ информаци¤

  void cycle();
};

#endif

#ifndef __TJOB_H__
#define __TJOB_H__

class TJob {
public:
  int jobID, jobDuration;
  TJob(int id, int duration) : jobID(id), jobDuration(duration) {
  };
};

#endif
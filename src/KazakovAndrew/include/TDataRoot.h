#ifndef __DATAROOT_H__
#define __DATAROOT_H__

template<class T>
class TDataRoot {
public:
  static const int kDefaultMemSize = 25;
  enum MemMode {
    MEM_HOLDER,
    MEM_RENTER
  };

  explicit TDataRoot(const int size = kDefaultMemSize);
  virtual ~TDataRoot(void);

  virtual bool IsEmpty(void) const;
  virtual bool IsFull(void) const;
  virtual void Put(const T&) = 0;
  virtual T Get(void) = 0;

  virtual void Print(void) = 0;

protected:
  T* memory_;
  int memory_size_;  // размер памяти для структуры данных
  int amount_;  // количество элементов в структуре данных
  MemType mode_;  // режим управления памятью

  void SetMem(void*, const int size);  // задание памяти
};

template<class T>
TDataRoot<T>::TDataRoot(const int size) {
  amount_ = 0;
  memory_size_ = size;

  if (memory_size_ == 0) {
    mode_ = MemMode::MEM_RENTER;
    memory_ = nullptr;
  } else {
    mode_ = MemMode::MEM_HOLDER;
    memory_ = new T[memory_size_];
  }
}

template<class T>
TDataRoot<T>::~TDataRoot() {
  if (mode_ == MemMode::MEM_HOLDER) {
    delete[] memory_;
  }

  memory_ = nullptr;
}

template<class T>
void TDataRoot<T>::SetMem(void* p, int size) {
  if (mode_ == MemMode::MEM_HOLDER) {
    delete[] memory_;
  }

  mode_ = MemMode::MEM_RENTER;
  memory_ = (T*)p;
  memory_size_ = size;
}

template<class T>
bool TDataRoot<T>::IsEmpty(void) const {
  return amount_ == 0;
}

template<class T>
bool TDataRoot<T>::IsFull(void) const {
  return amount_ == memory_size_;
}

#endif

#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "TDataRoot.h"

#include <iostream>

template<class T>
class TStack : public TDataRoot<T> {
public:
  explicit TStack(const int size = DefMemSize);

  virtual void Put(const T&) override;
  virtual T Get(void) override;

  virtual void Print(void) const override;

protected:
  int high_index_;
  virtual int GetNextIndex(const int index);
};

template<class T>
TStack<T>::TStack(const int size) : TDataRoot(size), high_index_(-1) {
  if (size < 0) {
    throw 4;
  }
}

template<class T>
int TStack<T>::GetNextIndex(const int index) {
  return ++high_index_;
}

template<class T>
void TStack<T>::Put(const T& val) {
  if (memory_ == nullptr) {
    throw 1;
  }

  if (IsFull()) {
    throw 2;
  }

  // Ипользуем функцию, а не просто ++high_index_, чтобы не переписывать
  // метод Put в классах-потомках
  high_index_ = GetNextIndex(high_index_);
  memory_[high_index_] = val;
  amount_++;
}

template<class T>
T TStack<T>::Get() {
  if (memory_ == nullptr) {
    throw 1;
  }

  if (IsEmpty()) {
    throw 3;
  }

  amount_--;
  
  return memory_[high_index_--];
}

template<class T>
void TStack<T>::Print() const {
  for (int i = 0; i < amount_; i++) {
    std::cout << memory_[i] << " ";
  }

  std::cout << std::endl;
}

#endif

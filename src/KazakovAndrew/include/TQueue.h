#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include "TStack.h"

template<class T = int>
class TQueue : public TStack<T> {
public:
  explicit TQueue(const int size = DEF_MEM_SIZE);

  virtual T Get(void) override;

  virtual void Print(void) const override;

protected:
  int low_index_;
  virtual int GetNextIndex(const int index) override;
};

template<class T>
TQueue<T>::TQueue(const int size) : TStack(size), low_index_(0) {}

template<class T>
int TQueue<T>::GetNextIndex(const int index) {
  return ++index % memSize;
}

template<class T>
T TQueue<T>::Get() {
  if (pMem == nullptr) {
    throw SetRetCode(DataNoMem);
  } else if (isEmpty()) {
    throw SetRetCode(DataEmpty);
  } else {
    int oldIndex = low_index_;
    low_index_ = GetNextIndex(low_index_);
    dataCount--;
    return pMem[oldIndex];
  }
}

template<class T>
void TQueue<T>::Print() void {
  for (int i = 0, j = low_index_; i < dataCount; j = GetNextIndex(j), ++i) {
    std::cout << pMem[j] << " ";
  }

  std::cout << std::endl;
}

#endif
